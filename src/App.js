import React, { useEffect, useState } from 'react'
import * as userAPI from './api/userApi'
import './App.css'
import Card from './components/Card'
import UserInput from './components/UserInput'
// import { useDispatch, useSelector } from './redux/react-redux'
import { useDispatch, useSelector } from 'react-redux'

function App() {
  const {
    data: users,
    loading,
    error,
  } = useSelector((state) => {
    return state.userState
  })
  const dispatch = useDispatch()
  const [editUser, setEditUser] = useState()

  useEffect(() => {
    dispatch({ type: 'GET_USERS' })
    userAPI
      .getUsers()
      .then((res) => {
        const action = { type: 'GET_USERS_SUCCESS', payload: res }
        dispatch(action)
      })
      .catch((error) => {
        dispatch({ type: 'GET_USERS_FAIL', payload: error })
      })
  }, [dispatch])
  return (
    <div className="w-[1200px] mx-auto">
      <UserInput editUser={editUser} setEditUser={setEditUser} />
      {!loading && !error && (
        <div className="flex flex-wrap ml-[-8px]">
          {users.map((user) => (
            <Card key={user.id} user={user} setEditUser={setEditUser} />
          ))}
        </div>
      )}
    </div>
  )
}

export default App
