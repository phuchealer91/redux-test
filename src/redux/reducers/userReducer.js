const initalState = {
  data: [],
  loading: false,
  error: undefined,
}
const userReducer = (state = initalState, action) => {
  switch (action.type) {
    case 'GET_USERS':
      return { ...state, loading: true }
    case 'GET_USERS_SUCCESS':
      return { ...state, data: action.payload, loading: false }
    case 'GET_USERS_FAIL':
      return { ...state, error: action.payload, loading: false }
    case 'CREATE_USER':
      return { ...state, loading: true }
    case 'CREATE_USER_SUCCESS':
      return {
        ...state,
        data: [action.payload, ...state.data],
        loading: false,
      }
    case 'CREATE_USER_FAIL':
      return { ...state, error: action.payload, loading: false }
    case 'UPDATE_USER':
      return { ...state, loading: true }
    case 'UPDATE_USER_SUCCESS':
      const newUser = state.data.map((item) => {
        return item.id === action.payload.id ? action.payload : item
      })
      return {
        ...state,
        data: newUser,
        loading: false,
      }
    case 'UPDATE_USER_FAIL':
      return { ...state, error: action.payload, loading: false }
    case 'DELETE_USER':
      return { ...state, loading: true }
    case 'DELETE_USER_SUCCESS':
      const newArr = state.data.filter((item) => item.id !== action.payload)
      return {
        ...state,
        data: newArr,
        loading: false,
      }
    case 'DELETE_USER_FAIL':
      return { ...state, error: action.payload, loading: false }
    default:
      return state
  }
}
export default userReducer
