import { createContext, useContext, useEffect, useState } from 'react'

const Context = createContext()

export const Provider = ({ store, children }) => {
  return <Context.Provider value={store}>{children}</Context.Provider>
}

export const useSelector = (selector) => {
  const store = useContext(Context)
  const [state, setState] = useState(() => selector(store.getState()))
  useEffect(() => {
    const unsubscribe = store.subscribe(() => {
      setState(() => selector(store.getState()))
    })
    return () => unsubscribe
  }, [store, selector])
  return state
}

export const useDispatch = () => {
  const { dispatch } = useContext(Context)
  return dispatch
}
