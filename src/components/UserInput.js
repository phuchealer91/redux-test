import React, { useEffect, useState } from 'react'
import * as userAPI from '../api/userApi'
// import { useDispatch } from '../redux/react-redux'
import { useDispatch } from 'react-redux'
const UserInput = ({ editUser, setEditUser }) => {
  const [name, setName] = useState('')
  const [avatar, setAvatar] = useState('')
  const createdAt = new Date().toISOString()
  const dispatch = useDispatch()
  useEffect(() => {
    if (editUser) {
      setName(editUser?.name)
      setAvatar(editUser?.avatar)
    }
  }, [editUser])
  const onHanleSubmit = async (e) => {
    e.preventDefault()
    if (editUser) {
      dispatch({ type: 'UPDATE_USER' })
      try {
        const data = await userAPI.updateUser({
          id: editUser?.id,
          name,
          avatar,
          createdAt,
        })

        dispatch({
          type: 'UPDATE_USER_SUCCESS',
          payload: data,
        })
      } catch (error) {
        dispatch({
          type: 'UPDATE_USER_FAIL',
          payload: error,
        })
      }
    } else {
      dispatch({ type: 'CREATE_USER' })
      try {
        const data = await userAPI.createUser({ name, avatar, createdAt })

        dispatch({
          type: 'CREATE_USER_SUCCESS',
          payload: data,
        })
      } catch (error) {
        dispatch({
          type: 'CREATE_USER_FAIL',
          payload: error,
        })
      }
    }
    setName('')
    setAvatar('')
    setEditUser(undefined)
  }
  return (
    <div className="my-4">
      <div>
        <form className="flex items-center" onSubmit={onHanleSubmit}>
          <div className="flex items-center">
            <label className="mr-2">Name:</label>
            <input
              type="text"
              value={name}
              name="name"
              className="border rounded py-1 "
              onChange={(e) => setName(e.target.value)}
            />
          </div>
          <div className="mx-4 flex items-center">
            <label className="mr-2">Avatar:</label>
            <input
              type="text"
              value={avatar}
              name="avatar"
              className="border rounded py-1 "
              onChange={(e) => setAvatar(e.target.value)}
            />
          </div>
          <button
            type="submit"
            className="bg-green-500 hover:bg-green-600 rounded py-1 px-3"
          >
            {editUser ? 'Update user' : 'Add user'}
          </button>
        </form>
      </div>
    </div>
  )
}

export default UserInput
