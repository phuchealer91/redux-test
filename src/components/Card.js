import React from 'react'
import * as userAPI from '../api/userApi'
// import { useDispatch } from '../redux/react-redux'
import { useDispatch } from 'react-redux'
const Card = ({ user, setEditUser }) => {
  const dispatch = useDispatch()
  const onHandleDelete = (id) => {
    dispatch({
      type: 'DELETE_USER',
    })
    userAPI
      .deleteUser(id)
      .then((res) => {
        dispatch({
          type: 'DELETE_USER_SUCCESS',
          payload: id,
        })
      })
      .catch((error) => {
        dispatch({
          type: 'DELETE_USER_FAIL',
          payload: error,
        })
      })
  }
  return (
    <div className="w-[calc(25%-8px)] ml-2 mb-4 ">
      <div className="shadow-md rounded">
        <div className="w-full pb-[62.5%] relative">
          <img
            src={user.avatar}
            alt={user.name}
            className="absolute object-cover w-full h-full"
          />
        </div>
        <div className="p-2">
          <h3 className="text-xl font-semibold text-gray-700">{user.name}</h3>
          <div className="mt-2 text-right">
            <button
              onClick={() => setEditUser(user)}
              className="bg-green-500 hover:bg-green-600 rounded py-1 px-3"
            >
              Edit
            </button>
            <button
              onClick={() => onHandleDelete(user.id)}
              className="ml-2 bg-red-500 hover:bg-red-600 rounded py-1 px-3"
            >
              Delete
            </button>
          </div>
        </div>
      </div>
    </div>
  )
}

export default Card
